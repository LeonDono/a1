# **Exercicis i preguntes LSSI**

http://www.lssi.gob.es/

1. Quins són els criteris bàsics per determinar si una web ha de complir l'LSSI?
   - La direcció i gestió del negoci ha d'estar centralitzada a Espanya.
   - Possessió d'una sucursal, oficina o qualsevol altre tipus d'establiment permanentment situat a territori espanyol.


2. Quins són els serveis exclosos de l'LSSI?

   Qualsevol, mentre que no es realitzi cap activitat econòmica ni es percebi ingressos derivat a publicitat o patrocinis.

   Tenen les seves pròpies lleis:
   - Ràdio.
   - Televisió.
   - Teletext
   - Correu electrònic no-comercial.



3. Quines són les obligacions dels que ofereixin algun servei per internet?
   - Estan obligats a retenir les dades de trànsit i connexions
   - Han de permetre saber com es el producte i quant val exactament.
   - Informar dels medis tècnics que es fan servir per garantir la seguretat.
   - Col·laborar alb l'autoritat.
   - Han de garantir  constància registral
   - Informar si es requereix autorització, informació sobre com s'ha obtingut i on.
   - Obtenir informació permanent, fàcil i gratuïta sobre l'empresa.
   - Protegir al consumidor.


4. Quines són les dades que ha de mostrar el propietari d'una web subjecta a l'LSSI segons l'obligació d'oferir informació?
   - Nom de l'empresa
   - Adreça
   - NIF
   - Forma de contacte(correu electrònic, telèfon o fax)
   - Dades d'inscripció registral
   - Codis de conducta als que estigui adherida


5. En intentar demanar informació sobre qui és el propietari de la web, ens han dit que només ho farà si li paguem 2€ perquè protegeix les seves dades personals segons la LOPD. És correcte? Quina sanció li podria caure?
   - Li pot caure una infracció lleu ja que vol rebre benefici, quan aquesta informació ha de ser gratuïta per tothom.
   - La sanció és fins a 30.000€


6. Volem comprar un producte que només està disponible en una web de Hong Kong. Però a l'hora de pagar no ens diu quines seran les despeses de transport. El podem denunciar per incompliment de l'LSSI?

  SI que es pot denunciar perquè es el que ha de fer una empresa que presta serveis a internet.


7. Defineix el “comerç electrònic”.

  Consisteix a la compra i venda de productes o de serveis a través de xarxes socials i altres pàgines web.


8. Indica cinc exemples d'activitats que creguis que es poden dur a terme mitjançant el comerç electrònic.

- **B2B**(Business to business):
Són les transaccions comercials que es fan entre empreses electròniques.
- **B2C**(Business to consumer):
Són les transaccions que fan els clients sobre la empresa electrònica que ven productes a internet.
- **B2E**(Busincess to employee):
És la relacó comercial que s'estableix entre una empresa i els seus propis empleats.
- **C2C**(Consumer to consumer):
És el que fa un consumidor/persona quan deixa d'utilitzar un objecte i el vol vendre.
- **G2C**(Government to consumer):
EL govern permet realitzar tràmits per part dels consumidor a través d'un portal.


9. Defineix “correu brossa (SPAM)” i comenta breument les sancions associades.

  És un missatge enviat per algun mitjà electrònic, de forma indiscriminada i massivament, sense cap consentiment de la persona que ho rep(receptor).
  La sanció es des dels 30.001€ fins els 150.000€.


10. Indica les propietats de la signatura digital.
- És un conjunt de dades electròniques, que poden ser medi d'identificació.


11. Explica la diferència entre signatura digital i signatura electrònica.
La diferència entre la signatura digital i electrònica, es:
- La firma electrònica tracta conjunts de dades de forma electrònica  que són utilitzats peridentificar a qui firma.
- La firma digital és un conjunt de caràcters que s'afegeixen al final d'un document o missatge per mostrar validesa i seguretat. Serveix per identificar al emisor del missatge.


12. Defineix el “correu segur”, explica'n breument les propietats.

  És l'encriptació aplicada als missatges de correu peruè ningú pugui llegir-lo en cas de interceptar el missatge, també serveix per que el remitent pugi saber qui es l'emisor de la communicació.


13. Indica tres exemples en els quals creguis que és necessari l'ús del correu electrònic segur. Explica'n breument el perquè.

- En el cas que el missatge tracti sobre dades sensibles d'una persona.
- Missatge dades personals de la gent.
- Contratació online, ha d'haber autentificació i no rebuig.

14. Quina relació hi ha entre el correu segur, la privadesa de l'usuari i la signatura electrònica?

  Les tres s'utilitzen per preservar la privacitat de l'usuari, per protegir les seves dades i saber de qui són (identificables).

# Exercicis i preguntes LPI


- Definiu els trets característics de cadascuna de les següents llicències, enllaçant un exemple un exemple d'obra que la usi:

  - Llicència propietària.
    - No es dóna accés al codi font
    - Limita el dret de distribució i ús.
    - No solen permetre modificacions.
    - Exemple: Windows utilitza aquesta llicència.

  - Llicència de codi obert.
    - Obliga a donar accés al codi font.
    - Es pot fer el que vulgui amb el programa.
    - Es pot canviar la llicència.
    - Exemple: Firefox, Ubuntu...

  - Llicència de software lliure.
    - Obliga a donar accés al codi font
    - Es pot fer el que vulgui amb el programa.
    - Exemple: Libreoffice, MySQL...

  - Llicència Creative Commons:
      - Només atribució (CC BY).
        - Copia i distribució a qualsevol medi de forma gratuïta.
        - Es pot transformar tot el que es vulgui, fins es pot comercialitzar.
        - S'ha de donar crèdit a l'obra original.

      - Atribució i compartir igual (CC BY-SA).
        - Copia i distribució a qualsevol medi de forma gratuïta.
        - Es pot transformar tot el que es vulgui, fins es pot comercialitzar.
        - S'ha de posar un enllaç a la llicència i indicar si s'han fet canvis.

      - Atribució i No Derivades (CC BY-ND).
        - Copia i distribució a qualsevol medi o format per a qualsevol finalitat, fins i tot comercial.
        - Si es transforma el material original no es pot difundir el material modificat.
        - S'ha de posar un enllaç a la llicència i indicar si s'han fet canvis.

      - Atribució i No Comercial (CC BY-NC).
        - Es pot copiar i redistribuir el material en qualsevol mdei o format
        - Es pot crear a partir del material original
        - NO es pot comercialitzar.

      - Atribució, No Comercial i Compartir Igual (CC BY-NC SA).
        - NO es pot comercialitzar
        - Es pot copiar i redistribuir de qualsevol medi o format.
        - Es pot crear a partir del material original.

      - Atribució, No Comercial i No Derivades (CC BY-NC-ND).
        - No es pot comercialitzar.
        - Si el material es va crear a partir del primer no es pot distribuir
        - Es pot distribuir de qualsevol medi o format

  - Domini Públic
    - Es distribuiex sense Llicència
    - Es pot fer el que vulgui amb el programa
    - Exemple: SQLite
