# Exercicis i preguntes LSSI

http://www.lssi.gob.es/

1. Quins són els criteris bàsics per determinar si una web ha de complir l'LSSI?
2. Quins són els serveis exclosos de l'LSSI?
3. Quines són les obligacions dels que ofereixin algun servei per internet?
4. Quines són les dades que ha de mostrar el propietari d'una web subjecta a l'LSSI segons l'obligació d'oferir informació?
5. En intentar demanar informació sobre qui és el propietari de la web, ens han dit que només ho farà si li paguem 2€ perquè protegeix les seves dades personals segons la LOPD. És correcte? Quina sanció li podria caure?
6. Volem comprar un producte que només està disponible en una web de Hong Kong. Però a l'hora de pagar no ens diu quines seran les despeses de transport. El podem denunciar per incompliment de l'LSSI?
7. Defineix el “comerç electrònic”.
8. Indica cinc exemples d'activitats que creguis que es poden dur a terme mitjançant el comerç electrònic.
9. Defineix “correu brossa (SPAM)” i comenta breument les sancions associades.
10. Indica les propietats de la signatura digital.
11. Explica la diferència entre signatura digital i signatura electrònica.
12. Defineix el “correu segur”, explica'n breument les propietats.
13. Indica tres exemples en els quals creguis que és necessari l'ús del correu electrònic segur. Explica'n breument el perquè.
14. Quina relació hi ha entre el correu segur, la privadesa de l'usuari i la signatura electrònica?

# Exercicis i preguntes LPI

http://www.mecd.gob.es/mecd/cultura-mecd/areas-cultura/propiedadintelectual/la-propiedad-intelectual/preguntas-mas-frecuentes/introduccion.html

- Definiu els trets característics de cadascuna de les següents llicències, enllaçant un exemple un exemple d'obra que la usi:

  - Llicència propietària.
  - Llicència de codi obert.
  - Llicència de software lliure.
  - Llicència Creative Commons:
      - Només atribució (CC BY).
      - Atribució i compartir igual (CC BY-SA).
      - Atribució i No Derivades (CC BY-ND).
      - Atribució i No Comercial (CC BY-NC).
      - Atribució, No Comercial i Compartir Igual (CC BY-NC-SA).
      - Atribució, No Comercial i No Derivades (CC BY-NC-ND).
  - Domini Públic
