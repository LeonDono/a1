# GESTIÓ DE PARTICIONS I FORMATAT


- Feu la pràctica amb qualsevol versió de Windows i Ubuntu Desktop.

- Abans de començar feu un "snapshot" o instantània de la maquina virtual del windows i el linux. Això farà que l'estat actual de la màquina virtual es guardi i pugueu fer tots els canvis que vulgueu sense por de fer malbé alguna cosa. Poseu-li de nom "Abans de particionat". En acabar, haureu d'esborrar aquesta instantània per tornar a l'estat anterior.

- En general, heu de comprovar SEMPRE que els canvis de configuració que feu produeixen l'efecte desitjat amb el conjunt de proves necessari.

- Feu captura de pantalla de les configuracions que apliqueu, documenteu el procés per la vostra referència posterior. Comproveu que tots els canvis funcionen i feu captures de pantalla que ho evidenciïn.

---

## Gestió de particions MBR en Windows

- A Windows les particions rebran lletres d'unitat (C: D: E: etc.)
- Deixarem només un disc SATA nou de 20GB per poder treballar amb ell amb la utilitat de gestió de discos.

### Gestió gràfica Windows

- Fes una captura del resultat final del disc. S'ha de veure el teu nom d'usuari. Utilitza l'eina "Administración de discos" per a crear un sistema de particionat MBR amb:
  1. **3particions (volums simples) al disc nou de 1000 MB cadascun i formatats amb NTFS de forma ràpida.**
  1. **Afegir una partició 4 amb 1000 MB formatat amb NTFS però sense format ràpid per a veure la diferència de temps de formatat (controla el temps i apunta-ho).** 2s Aprox.
  1. **La partició 4 és igual a les anteriors? Quina diferència hi ha?** No es igual ja que no es poden fer més de 3 primaries, llavors crea una partició extendida per fer lògiques.
  1. **Es poden continuar fent particions? Crea'n 3 més de 1000 MB.** Si que es poden fer perquè són lògiques.
  ![images](images1/1.png)
---
### Gestió des de consola Windows

- Captura les comandes que fas servir a la consola i com queda el disc al final del procés.
  1. Obre la consola de comandes i entra a la utilitat de gestió de particions "diskpart".
  2. Mira l'ajuda escrivint "help".
  3. Selecciona el disc.

    Per seleccionar el disc utilitzem la comanda `SELECT DISK=1`
  4. Selecciona i esborra la darrera partició creada abans en mode gràfic ("delete partition", "list partition" i "select partition=numero").
  ![image](images1/2.png)

  6. Comprova el resultat amb l'eina gràfica (refresca amb F5, si cal).
  ![image](images1/3.png)
---

## Gestió de particions MBR en Linux

- En Linux les unitats de disc SATA es diuen /dev/sda (primer disc), /dev/sdb (segon disc), etc.

- Les particions s'anomenen:
  - /dev/sdx1 (primera partició primària)
  - /dev/sdx2 (segona partició primària), etc. L
  - /dev/sdx4 serà la partició estesa
  - /dev/sdx5 serà la primera partició lògica, etc.


- Podeu llistar els dispositius i particions fent "fdisk -l".
![image](images1/4.png)

- Crearem un nou disc SATA de 8 GB per poder treballar amb ell amb la utilitat GPARTED.

### Gestió en entorn gràfic Linux

- Fes una captura del resultat final del disc. S'ha de veure el teu nom d'usuari. Utilitza l'eina Gparted.
  1. Selecciona el nou disc al desplegable d'unitats (surt "no assignat").
  2. Crea una partició primària de 1000 MB al disc dur (si et demana crear una taula nova de particions, que sigui de tipus msdos, que concorda amb una MBR).
  3. Posa-li etiqueta "p1" i sistema d'arxius NTFS. Quins sistemes d'arxius hi ha disponibles? Quin nom de partició te? Com s'hauria de dir?

    Sistemes d'arxius:`ext2, ext3, ext4, fat16, fat32, linux-swap, ntfs, net, sense formatejar`

    El seu nom de partició és: Partición nueva#1
    S'hauria de dir: /dev/sdb1
  ![image](images1/5.png)

  4. Ara crea una nova partició estesa amb la resta de l'espai lliure. Quin nom de partició te? Quin hauria de tenir?

    El nom de partició:Partición nueva#2
  Hauria de tenir: /dev/sdb2
  ![image](images1/6.png)

  5. Crea una partició lògica "p2" de 1000 MB amb format EXT4. Quin nom de partició te? Quin és el nom que hauria de tenir?

    Nom partició: Partición nueva#3
  Hauria de tenir: /dev/sdb4
  ![image](images1/7.png)

  6. Crea una partició lògica "p3" de 1000 MB amb format FAT32. Quin nom de partició te?

    El nom de partició és: Partición nueva#4
  ![image](images1/8.png)

  7. Intenta redimensionar la partició "p2" per que ocupi 2000 MB. Et deixa? Què hauràs de fer per que et deixi?

    No deixa perquè l'emmagatzematge màxim predeterminat és 1000MB i si es vol fer més gran s'han de moure les particions i deixar l'espai que es necessiti del disc lliure.

  8. Actualitza els canvis per a que siguin efectius. Fins ara, no s'han guardat.
  ![image](images1/9.png)

### Gestió en entorn consola Linux

1. Obre una consola i executa "sudo fdisk -l".
2. Quina informació obtens? Descriu-la amb les teves paraules.

   Mostra informació sobre els discs que hi ha al sistema.

3. Obre fdisk per treballar amb el disc dur nou.

  `sudo fdisk /dev/sdb`

4. Mostra l'ajuda de comandes de fdisk.
![image](images1/10.png)

5. Fes un llistat de particions de /dev/sdb.
![image](images1/11.png)

6. Esborra la partició "p1" i executa els canvis.
![image](images1/12.png)

7. Crea una nova partició "p4" dintre la partició estesa i guarda els canvis.
![image](images1/13.png)
---

- Torna a gparted (per visualitzar què has fet)
![image](images1/14.png)

1. Quan vas a gparted es veuen les noves particions? Si no, actualitza la pantalla de gparted refrescant els dispositius.

Si, es veuen les noves.

2. Quin sistema d'arxius té la partició p4? Formata la partició per tenir sistema d'arxius ext3.
![image](images1/15.png)

3. Esborra la partició estesa. Et deixa? Què hauries de fer per poder-ho fer? Actualitza els canvis i tindràs el disc buit de nou.

  No deixa, perquè hi ha particions dins d'aquesta.
  Per eliminar-la necessitem primer eliminar les que hi ha dins d'aquesta.
![image](images1/16.png)

4. Crea 4 particions primàries de 1000 MB.
![image](images1/18.png)

5. Crea una partició primària més. Et deixa? Què hauries de fer per poder continuar creant particions? Fes-ho.
  No es poden fer mes de 4 Primàries. Per fer-les hem d'eliminar una i crear una estesa(conta com a primària).
![image](images1/17.png)
---

1. Obre l'eina cfdisk.
![image](images1/19.png)

2. Obté la informació de les particions (UID, nom, tipus, etc).
![image](images1/20.png)
---

<!-- La siguiente práctica tiene el texto de consola escrito (sin captura) -->
