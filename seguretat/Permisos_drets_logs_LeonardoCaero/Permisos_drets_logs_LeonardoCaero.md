## **Permisos, drets i logs**


### **Passos per fer una instantània:**
1. Obrim la màquina virtual.
2. Fem click a l'opció Màquina, es pot trobar a la barra d'eines superior.
3. Seleccionem "fer una instantània" .

### **Creació d'usuaris**
Crearem 3 usuaris:
- joan
- albert
- pere

#### **Passos per arribar a Administració d'equips:**

##### Opció 1:
1. Cerquem al buscador de Windows "Administració d'equips" i fem click.

##### Opció 2:
 1. Premem la tecla Windows, o seleccionem el botó (situat de forma predeterminada a sota a l'esquerra).
 2. Cerquem "Panell de control".
 3. Dins del panell fem click a "Eines d'administrador".
 4. Per finalitzar fem click a "Administració d'equips".

A l'apartat d'usuaris, fem click dret i seleccionem "Fer un nou usuari".

En el nostre cas fem 3 usuaris amb la següent configuració:

<IMG src="Images/usuari_nou.png" width="200" height="200"/>

### **Opcions de seguretat i drets del sistema**

#### **Passos per arribar a Directives locals:**

##### Opció 1
1. Cerquem al buscador de Windows "Directives locals" i fem click.

##### Opció 2:
 1. Premem la tecla Windows, o seleccionem el botó (situat de forma predeterminada a sota a l'esquerra).
 2. Cerquem "Panell de control".
 3. Dins del panell fem click a "Eines d'administrador".
 4. Per finalitzar fem click a "Directiva de seguretat local".

#### Opcions habilitades:
- No mostrar el nom de l'ultim usuari que va iniciar sessió a la finestra d'inici.

 <IMG src="Images/no_mostrar_u_usuari.png" width="250" height="100"/>

- Nom del compte Administrador sigui "amagat".

 <IMG src="Images/nom_a_amagat.png" width="250" height="100"/>

- Text abans de iniciar sessió: "Equip destinat a empleats autoritzats", amb el títol "Atenció".

 <IMG src="Images/text_inici.png" width="250" height="100"/>

 <IMG src="Images/titol_inici.png" width="250" height="100"/>

#### Assignacions d'usuari:
- Dret apagar sistema (Joan) + treure grup usuaris.

 <IMG src="Images/joan_apagar.png" width="250" height="100"/>

 #### Comprovació:
 Es pot observar que l'usuari albert no té l'opció d'apagar el sistema.

![image](Images/comprovacio_albert.png)

- Canviar hora i data sistema (Joan) + treure grup usuaris.

 <IMG src="Images/joan_canvi_hora.png" width="250" height="100"/>

 #### Comprovació:
 Es pot observar que no pot canviar l'hora actual ni la data.

![image](Images/hora.png)

### **PERMISOS AL SISTEMA D'ARXIUS**
Primer hem de crear 2 carpetes a l'unitat c:\ :
- Alumnes
- Profes
Per crear-les hem de fer click dret al directori c:\ seleccionar "nuevo"-> carpeta.

<IMG src="Images/carpetas.png" width="500" height="200"/>

Fem els següents grups i usuaris:
- Usuaris: Pol, Maria, Roger.
- Grups: gPROFES, gALUMNES.

<IMG src="Images/usuaris.png" width="180" height="50"/>

<IMG src="Images/grups.png" width="300" height="50"/>

#### **Permisos:**
1. Posar a Roger dins el grup "gPROFES" i a Maria i Pol dins del grup "gALUMNES".

<IMG src="Images/gp_rog.png" width="150" height="180"/>

 <IMG src="Images/gal_m_p.png" width="150" height="180"/>

2. Donar a "gALUMNES" dret de lectura i escriptura a la carpeta "Alumnes". (Treure a "Usuaris" i "Usuaris Autentificats")

 <IMG src="Images/permisos_gal.png" width="150" height="180"/>

1. Doneu al grup "gPROFES" dret de lectura a la carpeta "Alumnes" i de lectura i escriptura a la carpeta "Profes".

 <IMG src="Images/permisos_gpro.png" width="150" height="180"/>

 <IMG src="Images/permisosAlu_gro.png" width="150" height="180"/>

#### **Permisos efectius de Roger, Maria i Pol**

Els permisos efectius d'en Roger sobre la carpeta "Alumnes" són només de lectura,ja que, com que es dins del grup "gPROFES" hereta els permisos del mateix.

Els permisos efectius de la Maria i Pol en aquest cas, són de Lectura i escriptura, ja que són dins del grup "gALUMNES" i hereten els mateixos permisos.

#### **Denegar permís d'escriptura a Maria sobre "Alumnes"**

Quan es denega el permís d'escriptura a Maria a carpeta
"Alumnes", segueix tenint els permisos d'escriptura i lectura ja que és dins del grup "gALUMNES".

#### **Permisos efectius de Roger sobre "Profes"**
Els permisos efectius de Roger sobre la carpeta "Profes" són:
- Lectura
- Escriptura

#### **Roger com a propietari de "Profes"**
El que he fet es anar a Propietats -> Seguretat -> Opcions avançades.
Quan arribes a opcions avançades hi ha un text que diu "cambiar" fem click i escollim a Roger.
Com que Roger es pripietari, ara té control total sobre la carpeta Profes.

<IMG src="Images/Roger_prop_prof.png" width="180" height="50"/>

#### **Inici sessió Roger + crear arxiu a alumnes i profes**
El que ha passat es que com Roger té control total pot crear l'arxiu, però a la carpeta Alumnes necessita permisos de l'administrador(per fer una carpeta), no pot fer un arxiu.
![image](Images/Roger_alm_arx.png)
![image](Images/ROger_arx_prof.png)

#### **Inici sessió Maria + crear arxiu a alumnes(editar)**
![image](Images/Maria_arx_alu.png)

#### **Inici sessió ROger + modificar arxiu Maria**
Si es pot modificar l'arxiu.

### **REGISTRE DE SUCCESSOS O LOGS**
- A "Aplicación" busqueu algun registre de l'aplicacio "defrag" i comproveu de que informa.

El que fa aquest registre es, com està explicat, "L'optimitzador d'emmagatzematge ha completat correctament l'optimització d'arrancada en (C:)"
![image](Images/defrag.png)

#### **Activació auditoria del sistema**
1. Anar a eines de directives de seguretat local -> Directives locals -> Directiva d'auditoria.
2. Activar "Auditar el canvi de directives".

<IMG src="Images/canvi_directives.png" width="150" height="180"/>

3. Activar "Auditar events d'inici de sessió"(per guardar al registre els intents d'inici de sessió tant correctes com incorrectes).

<IMG src="Images/auditar_events.png" width="150" height="180"/>

4. Activar "Auditar events del sistema" i "Auditar l'administració de contes".

<IMG src="Images/events_sistem.png" width="150" height="180"/>
<IMG src="Images/administracio_conta.png" width="150" height="180"/>

### **COMPROVACIÓ**

- Tanqueu i inicieu sessió (prova primer contrasenya incorrecta i després contrasenya correcta)
![image](Images/contrasenya_no.png)
![image](Images/Contrasenya_si.png)

- Canvieu l'hora del sistema (intenta-ho amb l-usuari joan i amb l'usuari albert)
![image](Images/canvi_hora.png)

- Crea un usuari nou anomenat "pere".
![image](Images/crer_pere.png)
