
## PERMISOS AL SISTEMA D'ARXIUS

- Per assignar permisos sobre el sistema d'arxius cal modificar les ACL de les carpetes i arxius (objectes o recursos) per afegir-hi els subjectes (usuaris i grups) que podran fer accions sobre ells. Per poder veure l'ACL d'un fitxer o carpeta aneu a "Boto dret->Propietats->Seguretat":
	- Podem veure **"Nombres de grupos o usuarios"** (subjectes).
	- A sota podem veure els **permisos que te el subjecte seleccionat** (Control total, Modificar, Lectura i execució, mostrar contingut de la carpeta, Lectura, Escriptura i Permisos especials).

	- Opcions avançades permet accedir a:
		- Permisos: Per veure tota la llista de subjectes i permisos de manera compacta. A mes permet controlar l'herència de permisos.
		- Auditoria: Per registrar tots els accessos al fitxer o directori.
		- Propietari: Per veure quin es l'usuari propietari i canviar el propietari (assignar a un altre o prendre possessió del directori o fitxer).
		- Permisos efectius: Per comprovar quins son els permisos que te un determinat subjecte sobre l'arxiu o carpeta:
			* Suma el permisos que té directament l'usuari sobre l'objecte.
			* Suma els permisos dels grups als que pertany l'usuari.
			* Resta les denegacions assignades a usuaris i/o grups, que prevalen sobre l'assignació de permisos.		

---

1. Creeu les carpetes "Alumnes" i "Profes" al directori arrel de la unitat c:
1. Creeu els grups locals "gPROFES" i "gALUMNES".
1. Creeu els usuaris Roger, Maria i Pol.


1. Poseu a Roger dintre el grup "gPROFES" i a Maria i Pol dintre el grup "gALUMNES".
1. Doneu a "gALUMNES" dret de lectura i escriptura a la carpeta "Alumnes" i treieu el grup "Usuarios" i "Usuarios autentificados" (no tindran cap permis). Haureu de tallar l'herència per poder treure els grups esmentats ("Opciones avanzadas->cambiar permisos->Incluir todos los permisos heredables del objeto primario de este objeto").
1. Doneu al grup "gPROFES" dret de lectura a la carpeta "Alumnes" i de lectura i escriptura a la carpeta "Profes".
1. Mireu el permisos efectius de Roger, Maria i Pol a la carpeta "Alumnes". Quins són? Treieu conclussions.

1. Denegueu a Maria el permís d'escriptura sobre el directori "Alumnes" i torneu a comprovar drets efectius sobre la carpeta. Canvia respecte l'anterior? Treieu conclussions.

1. Mireu els permisos efectius de Roger sobre la carpeta "Profes". Quins són?
1. Feu que la carpeta "Profes" sigui propietat de Roger. Com comproveu que ha passat?
1. Mireu els permisos efectius de Roger sobre la carpeta "Profes". Quins són? Han augmentat respecte els que tenia?

1. Inicieu sessió amb Roger i intenteu crear arxius als directoris "Profes" i "Alumnes". Què passa?
1. Inicieu sessió amb Maria, creeu i editeu un arxiu a la carpeta "Alumnes".
2. Inicieu sessió amb Pol. Intenteu modificar l'arxiu creat abans per Maria. Es pot? Mira quins drets hi ha sobre l'arxiu.

## REGISTRE DE SUCCESSOS O LOGS

- Per fer un seguiment dels successos produïts al sistema, normalment quan es produeix un error o un atac al sistema, farem servir el "Visor de eventos".

- El sistema registra tot el que se li demana registrar específicament (per defecte, el sistema operatiu registra successos predeterminats). Aneu a "Panel de control->Sistema y seguridad->Herramientas administrativas->Visor de Eventos" i deixeu-lo obert. Observeu:

	- El registre de successos (logs) de Windows esta organitzat en blocs:

		- "Registres de Windows": Registres que estaven disponibles en versions anteriors de Windows: els registres del sistema, seguretat i aplicació. Hi ha dos nous registres: de instalació i d'events reenviats (provenen d'algun altre equip de la xarxa).

		- Registre "Aplicación": Events registrats per aplicacions o programes. Ex: un programa de base de dades podría registrar un error d'arxiu.
		- Registre "seguridad": Guarda events com intents d'inici de sesió valids i no válids, a mes d'events relacionats amb us de recursos, com la creació, apertura o eliminació d'arxius i altres objectes.
		- Registre "instalación": Inclou events relacionats amb la instalació d'aplicacions.
		- Registre "sistema": Conte events registrats per components del sistema Windows. Ex: error al carregar un controlador o un altre component del sistema a l'inici que queda registrat al registre del sistema.
		- Registre "eventos reenviados": S'usa per emmagatzemar events recopilats d'equips remots.

- Per comprovar el funcionament del registre de successos i el visor de successos  i provarem el següent a la branca de "Registros de Windows":

	- A "Aplicación" busqueu algun registre de l'aplicacio "defrag" i comproveu de que informa.
 	- A "Seguridad": Abans de poder veure events, hem d'activar l'auditoria del sistema (Així Windows prendrà nota del que vulguem nosaltres).
	- Aneu a l'eina de directives de seguretat local->Directivas locales->Directiva de auditoria".
		- Activarem "Auditar el cambio de directivas".
		- Activarem "Auditar eventos de inicio de sesión" per que es guardin al registre els intents d'inici de sessio tant correctes com incorrectes.
		- Activarem "Auditar eventos del sistema" i "Auditar la administración de cuentas" per events correctes i incorrectes.

- Realitzeu les accions necessàries per que el sistema guardi constància al registre de successos de les accions:
	- Tanqueu i inicieu sessió (prova primer contrasenya incorrecta i després contrasenya correcta)
	- Canvieu l'hora del sistema (intenta-ho amb l-usuari joan i amb l'usuari albert)
	- Crea un usuari nou anomenat "pere".
	- Torneu al visor de successos i busqueu les evidencies de totes aquestes accions al registre de seguretat.
