# Document de mostra Markdown:
---

## Capçaleres


# Capçalera 1


## Capçalera 2


### Capçalera 3
#### Capçalera 4
##### Capçalera 5

---
## Format de text

-  _Això està escrit en cursiva_

- **Això està en negreta**

- _**Això està en negreta i cursiva**_

- ~~Això està tatxat~~

---
## Taules
- Ara introduiu una taula

Blancs | Blaus  
--|--
Estirada de corda  |  Rajoles
Construcció de gegants  | Descans  

## Llistes



### Llista ordenada amb subaartats

1. Colors
2. Blau
3. Groc
4. Persones
   1. Homes
   2. Dones

### Llista no ordenada amb subllistes

- Colors
  - Blau
  - Groc
- Persones
  - Homes
  - Dones

### Llista "checklist".
- - [ ] Colors
  - - [ ] Blau
  - - [ ] Groc
- - [X] Persones
  - - [ ] Homes
  - - [X] Dones

### Cites
Com va dir Confucio:

> Si un problema te solució, per que et preocupes? Si un problema no te solució, per que et preocupes?

### Enllaços a recursos externes (links)
[Click aqui per anar a google.](http://google.com) Això és text normal.

#### Enllaços a imatges

##### Imatge a internet

![image](https://www.pslab.eu/wp-content/uploads/2017/04/git.jpg)


Codi en la mateixa línea de text `var code = 1;`

- Aquí teniuun tros de codi en python:

```
x = 1
if x == 1:
    # indented four spaces
    print("x is 1.")
```
