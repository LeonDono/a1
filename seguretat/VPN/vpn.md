# OpenVPN amb PfSense



#### ATENCIÓ: Quan poseu les dades de la CA, les del servidor i les del client, feu servir les vostres dades personals per que es vegi a les captures que ho heu fet vosaltres.

## Configuració de servidor VPN: VPN->OpenVPN

### Instal·lació OpenVPN

  1. Anar al Package Manager, [System -> Package Manager -> Available Packages]
  2. Cercar OpenVPN i descarregar l'extensió
  ![](Imatges/vpn1.png)
  ![](Imatges/vpn2.png)

### Configuració VPN

  Per configurar l'VPN seguim els següents passos:

  1. VPN -> OpenVPN -> Wizards

  2. Seleccionem Local User Acces al desplegable.
![](Imatges/vpn3.png)

  3. Omplim amb les nostres dades els següents apartats (CA).
![](Imatges/vpn4.png)

  4. El següent apartat s'omple amb les dades anteriors de forma predeterminada, si no, omplim amb les nostres dades.
![](Imatges/vpn5.png)

  5. Omplim amb les dades desitjades, en aquest cas seleccionem UDP.
![](Imatges/vpn6.png)
![](Imatges/vpn7.png)

  6. En l'apartat tunnel, especifiquem la IP per el tunnel i la IP local.
![](Imatges/vpn8.png)

  7. Activem les IP dinàmiques en cas que no estigui activat prèviament.
![](Imatges/vpn9.png)

  8. Deixem l'apartat de firewall predeterminat (en aquest cas)
![](Imatges/vpn10.png)

  9. Quan acabem els passos arribem  a una finestra amb un missatge de "Instal·lació completada"
![](Imatges/vpn11.png)

## Creació de Certificate Authority (CA) i certificat Arrel: System->CErt.Manager.

En aquest cas ja no fa falta crear-ne un ja que l'hem creat prèviament amb el Wizard.
![](Imatges/vpn4.png)


## Crear regla al firewall

- Si no s'ha fet abans, cal obrir el port 1194 a la interfície WAN . Comprovem que apareixen les regles necessàries:
-
![](Imatges/vpn12.png)


## Crear usuari i certificat: System -> User Manager

- Pestanya Users: Hem de crear nou usuari que es connectarà via client VPN (a la base de dades interna del Pfsense)

  ![](Imatges/vpn13.png)


## Exportar el fitxer  de configuració del client:

1. Anem a OpenVPN -> Client Export Utility
  ![](Imatges/vpn14.png)

2. Per exportar (es fa desde la part inferior de la mateixa pàgina) fem click a "Inline Configurations -> Most Clients"
  ![](Imatges/vpn15.png)

3. El fitxer que baixem té la configuració del client VPN (alguna cosa semblant a això):

  ~~~
  dev tun
  persist-tun
  persist-key
  cipher AES-128-CBC
  auth SHA256
  tls-client
  client
  resolv-retry infinite
  remote 172.16.9.181 1194 udp
  verify-x509-name "Certificat Leo VPN" name
  auth-user-pass
  remote-cert-tls server

  <ca>
  -----BEGIN CERTIFICATE-----
  MIIEBTCCAu2gAwIBAgIBADANBgkqhkiG9w0BAQsFADBgMQ8wDQYDVQQDEwZDQSBM
  ZW8xCzAJBgNVBAYTAkVTMRIwEAYDVQQIEwlCYXJjZWxvbmExEzARBgNVBAcTCkdy
  YW5vbGxlcnMxFzAVBgNVBAoTDkxlbyBFbnRlcnByaXNlMB4XDTIwMDMxMDExNTE0
  NFoXDTMwMDMwODExNTE0NFowYDEPMA0GA1UEAxMGQ0EgTGVvMQswCQYDVQQGEwJF
  UzESMBAGA1UECBMJQmFyY2Vsb25hMRMwEQYDVQQHEwpHcmFub2xsZXJzMRcwFQYD
  VQQKEw5MZW8gRW50ZXJwcmlzZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
  ggEBAKF8Ehn0QYjv2W7deNIhWVXHvl2pDBH9/tU3ZLbXouFt3x/IIn9yP06GDIlq
  nk4Rr/8WuDd2I4obXAjEW9Txer+wWwVGfpanlsg9QbxKF66Lt29mOSJAEpKtRiCl
  LouD3GXu8CHJpp1CwEjVrc0SLHPIWyfKrFqA2j3Okxk3OGeCiSpE6Nty58cBcSSi
  93UxsiEbauPcfKhLk2FkV75WCd7GnvrE4c9HgeqGyWaTcoRZbcH/Qd2IfCLHseo0
  c0RV2+tU09nLaOme+VB3gmmUu6fsSst+b9r8dGKJRQ1khtl5lauC/iATh2o8rhqQ
  plW9p5Kr8hOO8zOm2mSPEETjJhMCAwEAAaOByTCBxjAdBgNVHQ4EFgQUwkjvWftF
  jMqX+NhsSfx3is6bl4owgYkGA1UdIwSBgTB/gBTCSO9Z+0WMypf42GxJ/HeKzpuX
  iqFkpGIwYDEPMA0GA1UEAxMGQ0EgTGVvMQswCQYDVQQGEwJFUzESMBAGA1UECBMJ
  QmFyY2Vsb25hMRMwEQYDVQQHEwpHcmFub2xsZXJzMRcwFQYDVQQKEw5MZW8gRW50
  ZXJwcmlzZYIBADAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0B
  AQsFAAOCAQEAEUGn1dtoEn/Wafy6ZdrWT74z+9/hFzmxEhpmfFoDlUzLZJC1LKa3
  Hh1iK1UrLEWeI8ERrDkuq5hzZP2rUTm011i8qE/W0VbDeeRl/s+6f/UlgvhboKpt
  5HvEjHg6fYP9Dz5kzV4mQWVrgxJMGt+4ompvlR1K8sCFXstrJ+W8N63ZE8UdDFSU
  Pr+jv3K1BGTrTGuolh9mS4+iW4hVJB3wDGKBm0oX9CMn0HNbaV3Iy6n2Rmueorud
  wcATgxBHsWTXUv6ajO2Co47hBvSgUMDys6C/gIKNYtxQRCRCd4dTZWOujISB8RpW
  nS76ABD24uzhux6aGwBRUgRrPetwQg4jCg==
  -----END CERTIFICATE-----
  </ca>
  <cert>
  -----BEGIN CERTIFICATE-----
  MIIEXzCCA0egAwIBAgIBAjANBgkqhkiG9w0BAQsFADBgMQ8wDQYDVQQDEwZDQSBM
  ZW8xCzAJBgNVBAYTAkVTMRIwEAYDVQQIEwlCYXJjZWxvbmExEzARBgNVBAcTCkdy
  YW5vbGxlcnMxFzAVBgNVBAoTDkxlbyBFbnRlcnByaXNlMB4XDTIwMDMxMTExMDAz
  N1oXDTMwMDMwOTExMDAzN1owYDELMAkGA1UEBhMCRVMxEjAQBgNVBAgTCUJhcmNl
  bG9uYTETMBEGA1UEBxMKR3Jhbm9sbGVyczEXMBUGA1UEChMOTGVvIEVudGVycHJp
  c2UxDzANBgNVBAMTBmxjYWVybzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
  ggEBAL1dacIpRLj0RU9emAj588mYKZVUK8H7HvVzT6cCLJbR3sXtUZVx/rHqBBj9
  7R1Pb5M1vh2VT+IvFuU8T+WucQREH7wqYqgIufPY/UhNhpUygxu0hl0422sdHtFg
  vFwxOn9NKyT4hB7tw3+a28QpDe4cIubYf+0FymARFi3kXVG7zQRu2g6OvxWEhdjS
  cNFkt5ozy2NZDFu+eZIdxt8zy34AbfzMpsmQXKmhIas1TNLIOwXXWFGMdsWoVrGA
  aMKz35UYplA4mRMJ+xovWpe03+/kqpUPwJvoE9YD3QFnfmCpAsXlDoFcbuR+F6BB
  i9X+Fc4NWxYQKbxRiJmLoQHQ2hsCAwEAAaOCASIwggEeMAkGA1UdEwQCMAAwCwYD
  VR0PBAQDAgXgMDEGCWCGSAGG+EIBDQQkFiJPcGVuU1NMIEdlbmVyYXRlZCBVc2Vy
  IENlcnRpZmljYXRlMB0GA1UdDgQWBBRf1jkGzLjo3ClhOGJKyGbpTeLlkzCBiQYD
  VR0jBIGBMH+AFMJI71n7RYzKl/jYbEn8d4rOm5eKoWSkYjBgMQ8wDQYDVQQDEwZD
  QSBMZW8xCzAJBgNVBAYTAkVTMRIwEAYDVQQIEwlCYXJjZWxvbmExEzARBgNVBAcT
  CkdyYW5vbGxlcnMxFzAVBgNVBAoTDkxlbyBFbnRlcnByaXNlggEAMBMGA1UdJQQM
  MAoGCCsGAQUFBwMCMBEGA1UdEQQKMAiCBmxjYWVybzANBgkqhkiG9w0BAQsFAAOC
  AQEAE7EQH7JC9MDe+KDKHDu2R5D7VEdvybtZ+s9GdWKgztvZn8aicOx+DJ+mjsvq
  y+JpOpUUNqFil3i+nlyEYRl+3k1leyxUfZiWdc68+wP87/SMIy0KDhPs0LL+MEUW
  /n5Z16YbXew/gCxD5fOglayIikErYbt7wL2RKTBPdPhFDvTIdaf9TbEENpRtGR8B
  6QfWGZqPsDZwWYZ8u/P8BDsxjdBMYfkW9P9GC/R+QJLZ4zPaA4dxPbGwgE/zZZvj
  UZVih6Kk/vgJUvO94KkEY9OiaIaN7D42x8POa8nwPx+ErH7hih0bqvRZ3V1wSOvC
  a1a4UlluGc5OrUFIKMA9zGLsIA==
  -----END CERTIFICATE-----
  </cert>
  <key>
  -----BEGIN PRIVATE KEY-----
  MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC9XWnCKUS49EVP
  XpgI+fPJmCmVVCvB+x71c0+nAiyW0d7F7VGVcf6x6gQY/e0dT2+TNb4dlU/iLxbl
  PE/lrnEERB+8KmKoCLnz2P1ITYaVMoMbtIZdONtrHR7RYLxcMTp/TSsk+IQe7cN/
  mtvEKQ3uHCLm2H/tBcpgERYt5F1Ru80EbtoOjr8VhIXY0nDRZLeaM8tjWQxbvnmS
  HcbfM8t+AG38zKbJkFypoSGrNUzSyDsF11hRjHbFqFaxgGjCs9+VGKZQOJkTCfsa
  L1qXtN/v5KqVD8Cb6BPWA90BZ35gqQLF5Q6BXG7kfhegQYvV/hXODVsWECm8UYiZ
  i6EB0NobAgMBAAECggEAP6+f9u3cL/vvJ39f0H0H0X/8VD2HreigA9qxKjDgACU7
  MAPdkU5SY1GZ6THVQitKEg9cDiT+Rcqif5wmHs/pMI8cwTLcFTlCGNGcdWmlXp4r
  W/D903Jg0QhtKevHzZW83717SibOgHxkaJETYkiQZ8lMr6iWdYeiBWcZt3t7KxZS
  /JFa9UxoSaTUFmDqdj/Gq7SyeAe43QZLohvrtD2ChXPF4w2MOBnHUr6Es2Qhx/pk
  y+BPgGklRVJEXByAaOATD00piBNraIJ+TAkLfU/sMkdafDcLiibDWAMo0QBjbQqT
  ZrGyoe8WimBozNHgXGj7W0pMNB5SYkYQjuREqc5wyQKBgQDhYpSmTBYs6FAHET/V
  iCHXbCNOzAjC4Ea8by6pBSzElP5bqdOId4xaODH703u55+JtlkxEbiu2gvZHTbyT
  umSFIt0Sy+TzRrzEehUepq+0t6ItWwkBgiv5uPrXQ6gdEbfbmSJjMe6kt1t2gsNY
  3wgxrLK3qXwgUdq2yEJZ77cjbQKBgQDXFkkyrAD9nLR7HligtVVbC+O+qKP74yaF
  ZIoGNr7JRXU0T5ChGDWQiwjr6NXXlR72DJuc1y/DisUugErV6SpQkcCi6k32Ybzb
  NiA1qlcADqBN0gWiYvw7k/nJMRpCo/ksdNcYfi5tC0pibYucqrqope2vxAfvKOHZ
  5uFI28b2pwKBgHhEXWP7t92QvZt31eNAuCY2uwSCFa6/dJ96iKCCdWgUxUHudkUc
  3dUKpYlmSWBjX0zw5ZOiYksKyRY1YgpGbPsqJmOwT2SNe48wQfLQKXQ6hRzEtiKK
  7qJb1eZmYN/aI4zXwyTBcUGN4g10K39RzBKNv/QHSYgJhbcBV36cTtJlAoGAZk99
  hhL3bavK1f27Xsai0Wo4bQq+5iRIwI7wcMA9xHO7qqRmQZrIb/cSyVS505BlZ9jb
  dKGPYM5zuQjhkBsXO7f5EAWwDBC1yvhWHfDm7WoHqrzKXVIEdTqQSLmIqkZhUlpJ
  rqbm4ovKWEOPc3hqzaoQA9mVhJfHxuTiKMwSOmUCgYEAgvhGL0ykE6WZGYgrk0wh
  LctIqbX7gxWh1LIqw6Zk2yy5vPcNCdRiK6dQmlL6XnQTC5wj1qS8yQ7sbo5CKx9l
  pIuWE9jYWofbDaUCcqFDbm4o6WyvTn+xkkcDTe0rVR1J/sJsurJL+9ndV+lMlD6d
  zjOratNzh4hJ6swCA2Gx2Lk=
  -----END PRIVATE KEY-----
  </key>
  key-direction 1
  <tls-auth>
  #
  # 2048 bit OpenVPN static key
  #
  -----BEGIN OpenVPN Static key V1-----
  2f94fd196c5addf2d503250592043fed
  74e2ae5b7c1833cf649a333b9e85bb32
  0eab928ca23ecf1a7ae704b8dfd20d7d
  bc1128717e29061cbfd8d739039dce0d
  4af7f5389b325bf1bcf9c56bf2a91c07
  3dabcf0bbdd420f40d3a9753a77b9f20
  5849de13c1f905dce785bff28731aa9e
  cc17576d39c1c443f7b1665d4837de9c
  3727dcb11cf58f7b083cbe58ec3c2538
  24cebb3a894b7e857f1e9bbc0ab0c48f
  820e8b9c4c80126e971248e5baf09f6e
  6bc121cef103a80eb4eba724c298724a
  0cb6231cc50f91f3f6412f73cf57954c
  4e9e282adf64f90748d49e498239846c
  ad84a166f62147633a3d5e66f0cd4c25
  83828f2262e0a6431e9cbbdf6b6d155a
  -----END OpenVPN Static key V1-----
  </tls-auth>
  ~~~

## Connexió al servidor VPN des de consola

### Instal·lació OpenVPN
Hem d'utilitzar la següent comanda.
~~~
LeonDono@lcaero:~$ sudo apt install openvpn
~~~

### Executar OpenVPN
Cal executar la comanda:
~~~
sudo openvpn --config "arxiu.ovpn"
~~~
~~~
LeonDono@lcaero:~/Escriptori$ sudo openvpn --config pfSense-UDP4-1194-lcaero-config.ovpn
Wed Mar 11 12:40:43 2020 OpenVPN 2.4.4 x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on May 14 2019
Wed Mar 11 12:40:43 2020 library versions: OpenSSL 1.1.1  11 Sep 2018, LZO 2.08
Enter Auth Username: lcaero
Enter Auth Password: ********
Wed Mar 11 12:40:50 2020 TCP/UDP: Preserving recently used remote address: [AF_INET]172.16.9.181:1194
Wed Mar 11 12:40:50 2020 UDP link local (bound): [AF_INET][undef]:1194
Wed Mar 11 12:40:50 2020 UDP link remote: [AF_INET]172.16.9.181:1194
Wed Mar 11 12:40:50 2020 WARNING: this configuration may cache passwords in memory -- use the auth-nocache option to prevent this
Wed Mar 11 12:40:50 2020 [Certificat Leo VPN] Peer Connection Initiated with [AF_INET]172.16.9.181:1194
Wed Mar 11 12:40:51 2020 TUN/TAP device tun0 opened
Wed Mar 11 12:40:51 2020 do_ifconfig, tt->did_ifconfig_ipv6_setup=0
Wed Mar 11 12:40:51 2020 /sbin/ip link set dev tun0 up mtu 1500
Wed Mar 11 12:40:51 2020 /sbin/ip addr add dev tun0 10.0.8.2/24 broadcast 10.0.8.255
Wed Mar 11 12:40:51 2020 Initialization Sequence Completed

~~~

## Connexió al servidor VPN des de Network Manager

- Per configurar al Network Manager, podeu importar directament el fitxer.

- Instal·leu al client el paquet network-manager-openvpn-gnome
- A la icona de Network Manager->VPN->símbol "+" (afegir)->Importa des d'un fitxer

- La pasarel·la és el servidor OpenVPN. Si piqueu Avançat, podeu configurar les opcions que al fitxer de configuració es troben al principi:

![](imgs/openvpn-pfsense-f06157b9.png)

![](imgs/openvpn-pfsense-c203e825.png)

  - Per provar la connexió aneu al Network Manager->Conexions VPN i escolliu la connexió creada.
  - Comproveu des d'un terminal que s’ha creat una interfície tun amb la ip de la xarxa virtual assignada (normalment 10.0.8.2).
  - Si no es connecta, podeu mirar els logs:
    - Al client: sudo tail -f /var/log/syslog | grep vpn
    - Al servidor: VPN->OpenVPN->icona “Related logs entries” a la part superior dreta de la pantalla. O bé, Status->OpenVPN.

  - Si tot va bé, haureu de poder fer ping a un equip de la xarxa local que hi ha darrera del servidor VPN (192.168.1.x) i també al servidor VPN en la seva adreça per defecte, 10.0.8.1 (si heu fet servir la xarxa 10.0.8.0 per les adreces virtuals de la VPN). Vigileu que el firewall permeti passar els protocols que feu servir per a les proves (Firewall->Rules->OpenVPN).

## Connexió VPN des de equips Windows

- Feu el mateix amb un client Windows 10 (MV amb interfície pont). S'ha d'afegir una nova connexió VPN i mirar si hi ha suport OpenVPN. Quines VPN suporta Windows 10?
- Instal·leu el client OpenVPN si no te suport propi.
