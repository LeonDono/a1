# **Política de contrasenyes en windows**



### Com arribar a la carpeta "Directivas de contraseñas"
Panell de control -> Eines administratives -> Directiva de seguretat local -> carpeta Directivas de contraseñas

### Directives:
- **Emmagatzemar contrasenyes de xifrat irreversible:** Proporciona la compatibilitat necessària per les aplicacions que necessiten contrasenya per autentificar(no es recomanada).
  - Exemple: Si aconsegueixes la clau de xifrat pots esbrinar les contrasenyes.
- **Exigir historial de contrasenyes:** Determina el número de noves contrasenyes que s'han de crear per a un usuari abans d'utilitzar l'antiga.
  - Exemple: Per seguretat has de canviar la contrasenya i després de fer-ne 3 noves pots repetir-hi la primera que vas posar.
- **La contrasenya ha de complir els requisits de complexitat:** Determina si les contrasenyes han de complir els requisits de complexitat.
  - Exemple: Escriure caràcters especials, números, majúscules...
- **Longitud mínima de la contrasenya:** Es el número mínim de caràcters que ha de tenir la contrasenya d'un usuari.
  - Exemple: Quan demanen una contrasenya amb un mínim de 10 caràcters.
- **Vigència màxima de la contrasenya:** Es el temps màxim que pots utilitzar una contrasenya abans que el sistema demani un canvi.
  - Exemple: Pots utilitzar la mateixa contrasenya durant un mes, però el sistema demana un canvi quan aquest període de temps arriba.
- **Vigència mínima de la contrasenya:** Es el temps mínim que el usuari ha de tenir la mateixa contrasenya abans de poder canviar-la.
  - Exemple: El sistema no permet cap canvi de contrasenya per part de l'usuari fins que arribi al mínim de temps.


### Configuració directives de contrasenya:

- Els usuaris en canviar la contrasenya no podran  posar cap de les 3 últimes contrasenyes que havien fet servir.
- Les contrasenyes han de complir els requisits de complexitat (indica quins són).
- Com a mínim han de ser de 10 caràcters.
- Cal canviar les contrasenyes una vegada al mes.
- Un cop canviada, no poden tornar a canviar la contrasenya abans de 3 dies.
- Els comptes s'han de blocar si s'intenta entrar en un compte 3 vegades amb contrasenyes incorrectes en menys de 2 minuts.
- Es desblocaran automàticament passats 5 minuts.

**Directiva contrasenyes:**

<IMG src="images/directiva_contrasenyes.png" width="200" height="60"/>


**Directiva bloqueig:**

<IMG src="images/directives_bloqueig.png" width="200" height="35"/>


### **Eines de gestió de contrasenyes**

**Instal·lació KeepassX:**
`sudo apt-get install keepassx`

**Manual d'ús usuari:**
**Creació de fitxer de contrasenyes**

1. Obrim el programa
2. Fem click a nova base de dades.
3. Click dret dins de l'arrel i afegim un nou grup per els grups de contrasenyes.
4. Dins del grup creat, fem click dret i seleccionem "afegir nova entrada".
5. Escrivim un nom per saber d'on es la contrasenya, afegim nom d'usuari i amb el botó de generar contrasenya ens permet fer una segura, combinada i llarga.
6. Col·loquem la URL d'inici de sessió.
7. Fer click dret i obrim la URL per comprovar que es correcte.

**Grups i comptes creats:**
- Compte gmail (Grup: email)
- Compte moodle (Grup: e-learning)
- Compte Imagine (Grup: programari)
- Compte App Inventor (Grup: e-learning)

**Comprovació Ubuntu:**

<IMG src="images/captura_contrasenyes.png" width="550" height="500"/>

**Comprovació Windows:**

<IMG src="images/Windows_keypass.png" width="550" height="500"/>
