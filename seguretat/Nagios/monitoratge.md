Leonardo Caero 2SMX A1


#Monitoratge de Xarxes

## Instal·lació Nagios

### Seguir les següents comandes:
~~~
sudo apt update && sudo apt upgrade
cd /tmp
wget https://assets.nagios.com/downloads/nagiosxi/xi-latest.tar.gz
tar xzf xi-latest.tar.gz
cd nagiosxi
sudo ./fullinstall
~~~
### Configuració Nagios

1. Obrim un navegador amb el client i escrivim la seva IP a la barra de cerca.

  ![](Imatges/2.png)

2. Seleccionem el llenguatge que vulguem aplicar al Nagios i la interfície desitjada.

  ![](Imatges/3.png)

3. A continuació configurem les opcions d'administrador.

  ![](Imatges/4.png)

4. La instal·lació finalitza amb aquest missatge

  ![](Imatges/5.png)

## Inventari Actius

1. A la pàgina principal, fem click a configurar monitoratge bàsic -> Tasques d'auto-descobriment.

  ![](Imatges/6.png)

2. Fem click a nova, i afegim la ip on volem actuar.

  ![](Imatges/7.png)

3. Dispositius connectats:

Dispositius|Descripció  
--|--  
10.0.2.2| telèfon IP  
10.0.2.3|  Desconegut
10.0.2.4| Linux Server
10.0.2.5|  Desconegut
10.0.2.7|  Desconegut
10.0.2.1| telèfon IP

![](Imatges/8.png)

## Supervisió de serveis i Dispositius

**Ex3:**

Configura un ‘dispositiu de xarxa genèric’ i fes servir l’adreça del servidor web que hi ha a la xarxa. Demostra que si el servidor s’atura, Nagios detecta que no pot fer PING i envia una notificació via correu electrònic.

### Passos:
1. Cerquem a l'apartat següent, i seleccionem dispositiu de xarxa genèric.

  ![](Imatges/14.png)![](Imatges/9+.png)

2. Posem la IP del servidor que es trobi a la xarxa, en aquest cas és "10.0.2.5"

  ![](Imatges/10.png)

3. Seleccionem les opcions de monitoreig.

  ![](Imatges/11.png)

4. Quan fem clck a finalitzar ens hauria de sortir un menú com el següent.

  ![](Imatges/12.png)

5. Per veure el resultat del dispositiu fem clock a "Veure detalls de l'estatus per [IP]"

  ![](Imatges/13.png)

#### AMB SERVIDOR ATURAT

Verificació per correu:

  ![](Imatges/15.png)

Nagios:

  ![](Imatges/16.png)


**Ex4:**
Busca quina configuració pot ser la més adient per monitora un servei web i com podem validar que el servei està actiu i funcionant correctament.

#### Passos
1. Seleccionem "Lloc Web" al wizard i posem la IP
  ![](Imatges/17.png)

2. Seleccionem els serveis que vulguem supervisar.
  ![](Imatges/18.png)

3. Seleccionem les configuracions del monitoreig.
  ![](Imatges/19.png)

4. Comprovació
  ![](Imates/20.png)

**Ex5:**
Verifica que aparegui un text dins de la web i si no apareix que enviï una notificació.

1. Comprovació
  ![](Imates/20.png)


**Ex6:**
Instal·la un agent al servidor web. Fes servir l’agent ncpa i segueix les instruccions d’instal·lació del mateix wizard de configuració.

#### Passos

1. Instal·lar agent al servidor monitoritzat

  1.2 Fer un wget a la carpeta de /tmp del link de l'agent ncpa.

  ![](Imatges/20.png)

2. Instal·lar el paquet amb la següent comanda:
   ```sudo dpkg -i ncpa-2.2.1.amd64.deb  ```
