# **OBTENCIÓ DE CONTRASENYES**

## **Comandes emprades**
- **Vgscan :** busca tots els grups de volums.
- **Vgchange :** canvia els attributs de grups de volums.
- **Lvs :** ensenya la infromacio sobre els vlums logics.
- **Mkdir :** make directory, crea un directori.
- **Mount :** monta un sistema de fitxers.
- **Fdisk :** per veure tots els dispositius disponibles.
- **cd :** change directory, permet canviar de directori.

Hem de copiar els fitxers [Password] i [shadow] al directori prèviament creat a part del disc.

## **Passos per crakejar les contrasenyes:**
 - **Unshadow :**

  Converteix el fitxer de shadow en un fitxer normal de passwords
~~~
root@kali:~/a# unshadow passwd shadow > r00t4john
Created directory: /root/.john
~~~  

 -  **Crack de passwords senzill:**

  En un aproximació que triga un temps reduït, fem "John" en mode Single.

  Comanda: `john --single r00t4john`
~~~
root@kali:~/a# john --single r00t4john
Warning: detected hash type "md5crypt", but the string is also recognized as "md5crypt-long"
Use the "--format=md5crypt-long" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 7 password hashes with 7 different salts (md5crypt, crypt(3) $1$ (and variants) [MD5 256/256 AVX2 8x3])
Press 'q' or Ctrl-C to abort, almost any other key for status
user             (user)
postgres         (postgres)
Warning: Only 4 candidates buffered for the current salt, minimum 24 needed for performance.
Warning: Only 7 candidates buffered for the current salt, minimum 24 needed for performance.
msfadmin         (msfadmin)
Warning: Only 5 candidates buffered for the current salt, minimum 24 needed for performance.
Warning: Only 17 candidates buffered for the current salt, minimum 24 needed for performance.
Warning: Only 7 candidates buffered for the current salt, minimum 24 needed for performance.
service          (service)
Warning: Only 18 candidates buffered for the current salt, minimum 24 needed for performance.
Warning: Only 17 candidates buffered for the current salt, minimum 24 needed for performance.
Warning: Only 22 candidates buffered for the current salt, minimum 24 needed for performance.
Warning: Only 5 candidates buffered for the current salt, minimum 24 needed for performance.
Warning: Only 6 candidates buffered for the current salt, minimum 24 needed for performance.
Further messages of this type will be suppressed.
To see less of these warnings, enable 'RelaxKPCWarningCheck' in john.conf
Almost done: Processing the remaining buffered candidate passwords, if any.
4g 0:00:00:00 DONE (2019-10-08 10:40) 36.36g/s 61690p/s 62200c/s 62200C/s dsys1903..dsys1900
Use the "--show" option to display all of the cracked passwords reliably
Session completed
~~~

 - **Crack de passwords incremental**

  Si volem fer el procés ràpid i sense diccionari, podem optar per la manera "brusca" (en aquest cas alpha, que indica només caràcters alfabètics).

  Comanda: `john --incremental:alpha r00t4john`
~~~
Warning: detected hash type "md5crypt", but the string is also recognized as "md5crypt-long"
Use the "--format=md5crypt-long" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 7 password hashes with 7 different salts (md5crypt, crypt(3) $1$ (and variants) [MD5 256/256 AVX2 8x3])
Remaining 3 password hashes with 3 different salts
Press 'q' or Ctrl-C to abort, almost any other key for status
batman           (sys)
1g 0:00:00:01  0.5524g/s 28813p/s 84623c/s 84623C/s juja..lear
Use the "--show" option to display all of the cracked passwords reliably
Session aborted
~~~
    He cancel·lat el procés ja que tardaba massa, i he fet el que ve a continuació amb regles i diccionaris.

 - **Crack de passwords amb regles i diccionaris**
  Executem John amb una llista molt completa (el procés és més lent). Atacarem amb la wordlist "rockyou.txt".
~~~
Warning: detected hash type "md5crypt", but the string is also recognized as "md5crypt-long"
Use the "--format=md5crypt-long" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 7 password hashes with 7 different salts (md5crypt, crypt(3) $1$ (and variants) [MD5 256/256 AVX2 8x3])
Remaining 2 password hashes with 2 different salts
Press 'q' or Ctrl-C to abort, almost any other key for status
123456789        (klog)
usuari           (root)
2g 0:00:00:33 DONE (2019-10-08 10:45) 0.06044g/s 88838p/s 88839c/s 88839C/s usucballs..usually12
Use the "--show" option to display all of the cracked passwords reliably
Session completed
~~~

    He descomprimit l'arxiu rockyou.tar.gz amb la comanda: `gzip -d rockyou.tar.gz`

 - **Taula amb contrasenyes crackejades**

  Per veure les contrasenyes s'ha  d'utilitzar la comanda `john --show r00t4john`
 ~~~
 root@kali:~/a# john --show r00t4john
root:usuari:0:0:root:/root:/bin/bash
sys:batman:3:3:sys:/dev:/bin/sh
klog:123456789:103:104::/home/klog:/bin/false
msfadmin:msfadmin:1000:1000:msfadmin,,,:/home/msfadmin:/bin/bash
postgres:postgres:108:117:PostgreSQL administrator,,,:/var/lib/postgresql:/bin/bash
user:user:1001:1001:just a user,111,,:/home/user:/bin/bash
service:service:1002:1002:,,,:/home/service:/bin/bash

  7 password hashes cracked, 0 left
 ~~~
