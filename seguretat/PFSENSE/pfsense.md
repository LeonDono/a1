<!--Leonardo Caero -->

## pfSense

### 1. Instal·lació pfSense

#### 1.1 Acceptar Copyright.
![](imatges/1.png)

#### 1.2 Escollir opció d'instal·lar.
![](imatges/2.png)

#### 1.3 Escollim el tipus de teclat que vulguem utilitzar, en aquest cas deixem el default.
![](imatges/3.png)

#### 1.4 Escollim la forma de particionar el disc, seleccionem Automàtica.
![](imatges/4.png)
![](imatges/4_1.png)

#### 1.5 Després de l'instalació ens demanará fer un reboot.
![](imatges/5.png)

### 2. Configuració inicial

#### 2.1 Configurar les interfícies.
Els passos a seguir són:

2.1.1 Selecció interfícies
~~~
Valid interfaces are:

em0     08:00:27:a5:97:a1   (up) Intel(R) PRO/1000 Legacy Network COnnection 1.
em1     08:00:27:67:10:d4   (up) Intel(R) PRO/1000 Legacy Network COnnection 1.

Do VLANs need to be set up first?
If VLANs will not be used, or only for optional interfaces, it is typical to say no here and use the webConfigurator to configure VLANs later, if required.

Should VLANs be set up now [y:n]? n
~~~

~~~
If the names of the interfaces are not known, auto-detection can be used instead. To use auto-detection, please disconnect all interfaces before pressing 'a' to begin the process.

Enter the WAN interface name or 'a' for auto-detection
(em0 em1 or a): em0
~~~

~~~
Enter the LAN interface name or 'a' for auto-detection
NOTE: this enables full Firewalling/NAT mode.
(em1 a or nothing if finished): em1
~~~

~~~
The interfaces will be assigned as follows:

WAN -> em0
LAN -> em1

Do you want to proceed [y:n]? y
~~~

2.1.2 Assignació adreces

Opció 2 Set interface(s) IP address
~~~
Available interfaces:

1 - WAN (em0 - dhcp, dhcp6)
2 - LAN (em1 - static)

Enter the number of the interface you wish to configure: 1
~~~

~~~
Configure IPv4 address WAN interface via DHCP? (y/n) y

Configure IPv6 address WAN interface via DHCP6? (y/n) y
~~~

~~~
Do you want to revert to HTTP as the webConfigurator protocol? (y/n) n
~~~

### Exercici 1. Configurar LAN
Opció 2
~~~
Available interfaces:

1 - WAN (em0 - dhcp, dhcp6)
2 - LAN (em1 - static)

Enter the number of the interface you wish to configure: 2
~~~

~~~
Enter the new LAN IPv4 address. Press <ENTER> for none:
>10.10.0.1/24
~~~

~~~
Do you want to enable the DHCP server on LAN? (y/n)
<div class="page-break"></div>
Enter the start address of the IPv4 client address range: 10.10.0.50
Enter the end address of the IPv4 client address range: 10.10.0.200
~~~

### Client

![](imatges/6.png)
![](imatges/8.png)

### 2. Assegurar l'entorn
