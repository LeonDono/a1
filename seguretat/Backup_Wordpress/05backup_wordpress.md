# Planificar còpia de seguretat d'un servidor WordPress


- Partirem d'una màquina virtual on instal·larem:
  - Un servei Apache per a servir pàgines web. Els fitxers html, php, css, javascript, etc. que conformen la pàgina web es troben a /var/www/...
  - Un servei de bases de dades per a contenir la BD necessària pel WordPress. El servidor de BD pot ser MariaDB o MySQL, per exemple. Podem fer una còpia de la BD amb mysqldump. Cal buscar com recuperar-la.

    - Podem guardar la base de dades copiant els fitxers que la composen (/var/lib/mysql). Això seria una còpia "binària" sense tenir en compte el contingut. Cal fixar-se que el servei ha d'estar instal·lat i ser la mateixa versió que teníem.
    - Podem guardar el contingut de la base de dades, fent servir PHPMyAdmin o millor la comanda mysqldump que guarda en un fitxer de text el contingut de la BD en format SQL (còpia interna). Aquest segon métode permet que es pugui migrar a d'altres versions del servei de BD amb més facilitat.


- L'ús del servei WordPress seria el següent:
  - Dona servei principalment de dia, tots els dies de la setmana. A partir de les 12 de la nit ningú hi sol accedir.

1. Respon les preguntes abans de començar.
2. Instal·la Wordpress en una màquina virtual, personalitza el WordPress i afegeix alguns articles.
3. Pensa en el cas de destrucció total de l'equip que conté el Wordpress, per un incendi, per exemple. Per tant, utilitzant els fitxers de la còpia de seguretat cal recuperar el servei WordPress en una màquina virtual nova.
4. Cal pensar la manera de fer còpia de seguretat automàtica i aplicar el model 3-2-1

---

# Preguntes a fer-se:

## 1. Fer còpia:

- **Quins tipus de dades anem a copiar?**
    La base de dades i els arxius de configuració

- **Quina metodologia de còpia utilitzarem segons les dades?**  
    Des de el phpmyadmin per les bases de dades (exportar) i en cas de arxius de configuració amb "cp" o "scp".

- **Què copiem?**
    Hem de copiar la base de dades que utilitzem a wordpress i la carpeta d'html amb tot el contingut.

- **Què no copiem?**
    El servidor apache, i el programari phpmyadmin

- **Amb quina freqüència copiem?**
    Depèn, recomanable un cop al dia.

- **En quin moment del dia copiem?**
    Seria recomanable al moment on menys es treballi. A la matinada si es possible.

- **Còpia manual o automàtica? (qui ho fa o quin sistema automàtic)?**
    De forma automàtica, és més eficient i ràpid. Amb el crontab. Amb scripts

- **Qui fa les còpies, les controla, les transporta?**
    Les copies les fa el sistema, l'administrador controla aquestes i el seu mètode de transport és fa de forma automàtica

- **Quant (GB) ocupen les dades que copiem?**
    40KB dels arxius de wordpress i 3-4KB de la base de dades

- **Quant ocupa (GB) el total de còpies que guardem?**
    Depèn de la mesura de les seves dades del que vulguem fer còpia. En el meu cas són 3 còpies (uns 200KB)

- **Quant de temps disposem/necessitem per a fer la còpia?**
    Depèn de la disposició.

- **Quins suports fem servir?**
    Disc durs i al núvol

- **Tenen data de caducitat els suports?**
    Es desconegut.

- **Quan es restauri existiran els dispositius de lectura?**
    Si

- **On guardem físicament els suports?**
    De forma externa.

- **Cal xifrar les dades?**
    Depèn de quines dades siguin. En el cas que siguin dades sensibles si.

- **Cal comprimir les dades?**
    Per ocupar menys espai si.

- **Quin tipus de còpia fem? (completa, diferencial, incremental)**
    Farem còpies incrementals.

- **Com sabem si s'ha fet bé la còpia?**
    Realitzant una comprovació amb els hash de la original y la còpia.

- **Com es registra en documents les còpies fetes?**
    Amb scripts.

- **Fins quan seran vàlides les còpies?**
    Fins els 5-6 mesos com a màxim.

## 2. Restaurar còpia

- **Quins tipus de dades anem a restaurar.**
    Els arxius de configuració de wordpress i la base de dades.

- **Com sabem que el backup està bé abans de restaurar?**
    Comparant els hash.
- **Qui pot restaurar?**
    L'administrador del servidor.

- **On restaurem?**
    Al suport objectiu (extern)

- **Com es restaura la còpia?**
    Fent una còpia dels arxius al lloc on han d'estar.

- **Podem restaurar parcialment o ha de ser total?**
    Es pot restaurar parcialment.

- **Quant trigarem a restaurar?**
    Depèn de la quantitat de dades. En aquest cas poc.


## 3. Script per fer còpia de seguretat:
~~~
#!/bin/bash

sudo tar -cvf /home/usuari/backups/backup-$(date +%y-%m-%d).tar /var/www/html/wp
scp -r /home/usuari/backups/backup-$(date +%y-%m-%d).tar 172.16.9.214:/home/usuari/backups
mysqldump -u root -pP@ssw0rd lcaero > backups/db-$(date +%y-%m-%d).sql
scp -r backups/db-$(date +%y-%m-%d).sql 172.16.9.214:/home/usuari/backups/db-$(date +%y-%m-%d).sql
~~~

## 4. Crontab:

~~~
# m h  dom mon dow   command
00 08 * * * ./home/usuari/backups/backup.sh
~~~
