# Exercicis legislació

---
- Referències:
  - Agència Espanyola de Protecció de Dades: https://www.aepd.es/
  - Autoritat Catalana de protecció de dades: http://apdcat.gencat.cat/ca/inici
  - Unió Europea: https://ec.europa.eu/info/law/law-topic/data-protection_es
  - LOPD: https://www.boe.es/buscar/doc.php?id=BOE-A-2018-16673
---

## Delictes

1. Fes un quadre resum on constin (Consulta el codi penal si tens dubtes ):

  1. Tipus de delictes informàtics
  1. Definició
  1. Exemples de delictes d'aquest tipus

---

## Protecció de dades de caràcter personal

1. Què volen dir les sigles RGPD?

1. Quina motivació té la RGPD?

1. Per què s'ha redactat la LOPD? Què volen dir les sigles?

1. A la web de l'Autoritat Catalana de Protecció de Dades, obté les definicions dels següents conceptes clau i escriu-les amb les teves paraules (NO copiar i enganxar):

  - Dades personals
  - Fitxer
  - Tractament de dades
  - Interessat
  - Responsable del tractament
  - Encarregat del tractament
  - Delegat de protecció de dades (DPD)
  - Consentiment de l'interessat
  - Drets de l'interessat
  - Violació de seguretat
  - Dades de consideració especial i tipus.
  - Anàlisi de riscos
  - Drets dels interessats

1. Un client nostre que es dedica a la venda de videojocs per internet ens ha demanat que l'orientem respecte la LOPD. Nosaltres buscarem la informació a la web de l'Autoritat Catalana de Protecció de Dades.

  1. Explica al client quines obligacions tindrà com a encarregat del tractament a partir del moment que reculli dades personals dels seus clients.
  1. El nostre client ha pensat en demanar dades personals que ara mateix no necessita per que en el futur vol oferir serveis personalitzats als clients. Podrà fer-ho? Per què?
  1. Explica quins drets tindran els seus clients en quant a les dades de caràcter personal.
  1. Busca models dels documents necessaris per a informar als clients quan es recullen les seves dades personals. Aquests documents han de estar d'acord a la RGPD.
     - Les dades personals que es recullen són nom i cognoms, adreça, telèfon, número de targeta de crèdit.
     - Cal informar al client sobre els seus drets en el moment de la recollida de les dades.
     - Cal preveure com podrà exercir el client els seus drets reconeguts al RGPD.
