# **Exercicis legislació**


## **Delictes**

### **Què és un delicte?**
Un delicte és una conducta típica, antijurídica, culpable i per tant subjecte a càstig.

- **Delicte civil:** És un acte il·lícit amb l'intenció de danyar als altres.
- **Delicte penal:** És un acte que està tipificat en el Codi Penal.

**1.** Fes un quadre resum on constin (Consulta el codi penal si tens dubtes ):

| Tipus de delicte | Nº Article | Definició | Exemple |
|:----------------:|:----------:|:---------:|:-------:|
| Intrusió informàtica | 197bis | L'accés o facilitació d'accés vulnerant mesures de seguretat i sense autorització. | Accedeix algú sense cap permís a unes dades passant les mesures de seguretat. |
| Interceptació de transmissions de dades informàtics | 197bis ap.segon | Interceptar transmissions no públiques de dades informàtics. | Sniffers, Keyloggers, Troians... |
| Amenaces | 169 | Amenaces realitzades per qualsevol mitjà de comunicació. | Qualsevol amenaça realitzada per un mitjà de comunicació. |
| Delictes contra l'honor: Calumnies i injuries | 205 i ss | Calumnia: Acusar algú d'haber comès un delicte sabent que és fals.  Injuria: Lesionar la dignitat o l'honor d'una persona. | Calumnia: acusar a algú sabent que és fals  Posar una fotografía inadequada d'alguna persona. |
| Fraus informàtics | 248 | Quan s'enganya a algú perquè faci alguna cosa que el perjudici. | Phising bancari. |
| Sabotatge informàtic | 264 | Producció, venda, distribució, exhibició o possessió de material pornográfic de menors. | Producció de material pornográfic infantil. |
| FALSIFICACIÓ | 189 | Alterar alguna cosa per fer-la passar per autèntica. | Possessió de software informàtic per cometre delictes de falsedat. |
| Descobriment o revelació de secrets | 197 i ss | Divulgar informació d'una altre persona. | Revelar un secret d'una altre persona (fer-lo públic) |

</center>

<!---
Comentar: <!--- text -->

<!---
Centrar un cuadre, text... "</center>"
-->

---

## **Protecció de dades de caràcter personal**

1. Què volen dir les sigles RGPD?

  Les sigles volen dir: **R**eglament **G**eneral de **P**rotecció de **D**ades.

1. Quina motivació té la RGPD?

  La motivació de la RGPD es per respectar el tractament de les seves dades personals i la llibertat de circulació d'aquestes.

1. Per què s'ha redactat la LOPD? Què volen dir les sigles?

  Les sigles volen dir: **L**lei **O**rgànica de **P**rotecció de **D**ades.

  Es va redactar per garantir i protegir les dades personals, les llibertats públiques i els drets fundamentals per les persones, especialment de l'honor, intimitat i privacitat personal.

1. A la web de l'Autoritat Catalana de Protecció de Dades, obté les definicions dels següents conceptes clau i escriu-les amb les teves paraules (NO copiar i enganxar):

  - **Dades personals:** És qualsevol informació d'una persona identificada o identificable

    `Persona identificada: Es una dada persona, qualsevol informació sobre la persona, ara com: el nom, cognom, DNI... `
  - **Fitxer:** qualsevol conjunt que sigui estructurar amb les dades personals accessibles.
  - **Tractament de dades:**
  - **Interessat:** És qualsevol persona individual que pot ser identificada de forma directa o indirecta a través d'identificadors (Nom,Cognom,DNI...)
  - **Responsable del tractament:** la persona o organisme que determina les finalitats i els mitjans que s'utilitzaran del tractament.
  - **Encarregat del tractament:** És la persona o organisme que tracta les dades personals únicament per compte del responsable del tractament.
  - **Delegat de protecció de dades (DPD):** És el responsable de supervisar com es tractaran les dades personals.
  - **Consentiment de l'interessat:** Qualsevol acció inequívoca i/o explícita per la qual l'interessat accepta.
  - **Drets de l'interessat:**
  - **Violació de seguretat:** Accions que ocasionin la destrucció, la pèrdua o l'alteració de les dades personals.
  - **Dades de consideració especial i tipus:**
  - **Anàlisi de riscos:** Es la valoració portada a terme per els responsables sobre els riscos dels tractaments que realitzaran. Posen mesures que s'han d'aplicar i com fer-ho.

1. Un client nostre que es dedica a la venda de videojocs per internet ens ha demanat que l'orientem respecte la LOPD. Nosaltres buscarem la informació a la web de l'Autoritat Catalana de Protecció de Dades.

  1. Explica al client quines obligacions tindrà com a encarregat del tractament a partir del moment que reculli dades personals dels seus clients.
  1. El nostre client ha pensat en demanar dades personals que ara mateix no necessita per que en el futur vol oferir serveis personalitzats als clients. Podrà fer-ho? Per què?
  1. Explica quins drets tindran els seus clients en quant a les dades de caràcter personal.
  1. Busca models dels documents necessaris per a informar als clients quan es recullen les seves dades personals. Aquests documents han de estar d'acord a la RGPD.
     - Les dades personals que es recullen són nom i cognoms, adreça, telèfon, número de targeta de crèdit.
     - Cal informar al client sobre els seus drets en el moment de la recollida de les dades.
     - Cal preveure com podrà exercir el client els seus drets reconeguts al RGPD.
