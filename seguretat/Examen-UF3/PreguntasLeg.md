# Preguntas

Reglament general:
que vol dir
objectius lssi:(motivació)
Llei de serveis de la societat de l'informació i del Comerç Electronic
Regula l'activitat de les organitzacions que treballen en la societat de la Informació

Es posen regles per evitar els enganys als usuaris que volen comprar via internet.

Qui esta obligat a LSSI:
Qualsevol que faci algun tipus d'activitat economica per xarxes de telecomnicacnions. Sempre que tinguialguna seu  a l'Estat Espanyol.
Qualsevol cosas que comporti ingressos: DIrectes O Indirectes.

La publicitat i la promcio es consideren activitats comercials.


Regula el comportament dels prestadors de serveis.

Què és un prestador de serveis? és algú que obté beneficis d'un servei que ofereix a una persona.

Defineix les regles de la publicitat electrònica.
Estableix com ha de ser la contractació electrònica.

---

obectius RGPD
Reglament General de Protecció de Dades.
Els objectius son:
- Preservar los derechos de una persona fíisca.
- El tratamiento de datos ha de ser percibido por la humanidad
- Conseguir una regulacion mas uniforme
- Garantir la libre circulacion de datos
- Reforçar al seguretat juridica  i generar seguretat.

Cuando se aplica el RGPD:
En el caso de que trate de personas fisicas, sin excepcionses (tratamiento de datos.)
EN el caso de que se obligue a todas las empresas i organismos que gestionan los datos perosnales.

NO se aplica cuando:
- Son datos relativos a personas juridicas (empresas)
- Son ficheros o conjuntos de ficheros acorde con los criterios especificos.
- Actividad exclusivamente personal o domestica (no comercial o profesional)

Ámbito territorial:
- Obliga a los responsables o encargados del tratamientos si estan situadoes en la Union Europea.


**Principio de reponsabilidad proactiva:
- El responsable ha de cumplir la normativa.
- Esta obligado a aplicar medidas de seguridad tecnicas i organizativas de forma apropiada
- Garantizar o poder demostrar el cumplimiento.
- Ha de tener una actitud consciente , digligente i proactiva por parte de las organizaciones.**

Enfoque de avaluacion de riesgos:
Tener en cuenta la naturaleza, el ambito, contexto i la finalidad del tratamiento.
- Evaluar el riesgo (derechos y libertades de las personas)
- Licitud, lealtad i TRANSPARÉNCIA.
- minimacion de datos.
- Confidencialidad, integridat, disponibilidad i resiliencia.

Consentimiento:
El interesado ha de dar los datos declarando de forma afirmativa clara, sin errores.

QUe no es un consentimiento valido:
- Las casillas ya marcadas previamente de forma automatica.
- El consentimiento tàcito o innaccion. (consentiment que no es per part de l'interessat, sino por la falta de su actuacion)

Consentimiento de menores.
- Solo es valido si tienen 16 (14?) o más.
- En los estados que sean miembros de la Union Europea pueden rebajar la edad hasta los 13 años.
- El lenguaje que se utiliza para informar-los ha de ser entendible por ellos.

SI los datos de una persona han sido afectados tiene el derecho de informacion

Derecho de acceso:
- El interesado puede obtener una confirmacion en el caso que esten utilizando sus datos.
- Puede tener acceso a sus datos i a la informacion de tratamiento.
- Obtener una copia de seguridad de sus Datos
- Posibilidad de proporcionar un acceso remoto i seguro a los datos de forma iterna.

El derecho de portabilidad es una forma avanzada del derecho de acceso.


Derechos a la supresion de datos:
- Tiene el derecho de suprimir los datos personales que le conciernen.
- puede dar derecho de la limitacion o bloqueo de datos cuando:
  - Existe un proceso de reclamacion
  - Finalidades legitimas

Delegado de proteccion de datos.
DPD
es el encargado del tratamiento de las obligaciones legales en la materia de protección de datos de la entidad o empresa contratada.

ARCO:
Acceso Rectificacion Canelacion  Oposicion.


**Datos personales:
Un dato personal es aquella información que se tiene de una persona física identificada o identificable (DNI, nombre, apellidos...)**

**Categorias especiales de los datos personales:
- Revelar l'origen ètnico o racial.
- Opiniones políticas o convicciones religiosas
- Afiliació sindical
- Tratamiento de datos geneticos, biometricos (lector de huellas, lector de pupila)
- Datos relativos a la salud de la personal
- Datos relativos sobre la vida sexual o sus orientaciones sexuales.**

Quan va entrar en vigor? Va entrar en vigor a maig de 2016.
---
LPI
Llei de propietat intel·lectual.

perque es van crear
Es va  crear la LPI per protegir les creacions de la ment humana. (drets d'autor)

Propietat intel·lectual. fa referència a les creacions de la ment humana.

Hi ha 2 formes de proteccio:
- Drets d'autor
- Propietat industrial (invencions, patents, maques...)

El que intenta la propietat industrial es evitar que els consumidors s'equivoquin a l'hora de comprar un producte.

Què protegeixen els drets d'autors:
- Obres escrites
- Obres musicals
- Obres artistiques
- Multimeda
- Obres dramàtiques
- software

Objectiu del drets d'autors:
- Aconseguir que l'autor pugui gaudir de la seva obra.
- No protegeixen idees, sino l'implementacio que se'n fa
- La condiciio es que l'obra ha de ser la original.
- Copiar una obra o parts importants d'aquesta. Es un plagi i és delictes.
- No importa la qualitat de la creacio.

Qui obté els drets d'autors:
- Els drets s'otorguen a l'autor de l'obra.
  - És reben de forma immediata.
- Si hi ha més d'un autor, tots aquests reben els drets d'autor.
- La possiblitat de divulgar l'obra es de 70 anys des de la mort de l'autor.
- Es fa servir el simbol de copyright per indicar que una obra es protegida.
  - Encara queno estigui aquest simbol l'obra es protegida igualment.
  - Els drets es reben a l'hora de fer l'obra.

Tipus de drets d'autors:
- Drets morals: son irrenunciabes
- **Drets d'explotacio: Es poden vendre o cedir a un altre.**

Drts Morals:
- Dret a ser reconegut
- Dret a garantir integritat de l'obra.
- Dret a divulgar l'obra.
- Dret de accedir a l'exemplar unic en el cas de que s'hagi venut.
- No caduquen mai.

Drets Explotacio:
- Reproduccio obra, fer copies.
- Distribucio de l'obra.
- Després dels 70 anyes de la mort de l'autor passa a ser domini públic.

Copia privada:
- Es pot fer ua copia privada en el cas de que s'hagi accedit de forma legal. També en el cas de que no es faci servir per activitats lucraties ni per perjudiciar a tercers.
- Nomes es poden fer  en suport fisic.

Qui pot fer servir l'obra:
- Cal tenir el permis de l'autor.
- Pirateria: fer copies no autoritzades d'obres protegides.
- Es poden tancar webs i imposar multes.

**Llicencies:
- Creative Common
- Copyleft: Permet fer servir, copiar, alterar l'obra de forme lliure.**

**Codi obert:
- Obliga a donra acces al codi font.
- Es pot fer el que vulgui amb el programa.
- Es pot canviar la llicencia.(MAC OS, té parts de freeBDS)

Programari lliure:
- Obliga a donar accñes al codi font.
- Es pot modificar el programa pero NO es pot canviar la llicencia.**

Domini public: Es distribueix sense llicencia, es pot fer el que vulgui amb el programa.


`LSSI: hacer que la gente confie en que los datos almacenados esten seguros. regulel comportamiento para la gente que vende (ejemplo)

RGPD: normas que aplicar en el caso de usar datos personales. obligaciones, derechos del otro, que he de hacer.

LPI:derechos de autor, como se protegen.`




que es el responsable de tratamiento
**el principal responsable de garantizar el cumplimiento del RGPD en cuanto a la recopilación, gestión, acceso y revocación de los datos personales.**

**encargado:debe garantizar que no utilizará los datos personales para un fin distinto al descrito por el responsable.**

**dpd: la persona encargada informar a la entidad responsable o al encargado del tratamiento sobre sus obligaciones legales en materia de protección de datos.**

Anàlisi de riscos, comprovar si els tractaments tenen riscos, en cas de violacions de seguretat donar part de les dades a les autoritats.

TEST: 20 Preguntas
5 PREGUNTAS,
cuales son las obligaciones de ...
si cuelgo banners publicitarios**
