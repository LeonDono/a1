#**Servidor Windows Server 2016**

Instal·la Windows Server 2016 a un nou equip. Fes una comparativa de les diferències entre les versions standard i datacenter. Indica els requisits mínims i recomanats de la versió que has instal·lat.


Funció | DATACENTER | STANDARD
---|---|---
Funcionalitat de core  |Disponible   |DIsponible  
Contenidors Hyper-V  |Il·limitats   |2  
Contenidors windows server  |Il·limitats   |Il·limitats  
Nano server  |Disponible   | Disponible  
Espai directe i recplica de l'emmagatzem  | Disponible   | No Disponible  
  Aplicació de xarxa| Disponible  | NO Disponible   
Preu  |5.510 EUR   | 790 EUR

`Hyper-V: és un programa de virtualització de Microsoft basat en un hipervisor  pels sistemes de 64 bits.`

`Hipervisor: és una plataforma que permet el control de virtualització per utilitzar al mateix temps diferents sistemes operatius en el mateix ordinador.`

**Requisits mínims:**

Descripció|Requisits mínims del sistema
---|---
Arquitectura CPU  |  x64
CPU clock rate  | 1.4 GHz  
RAM  | 512MB
Espai de disc  | 32GB
Adaptador de xarxa  | 1x Ethernet (si es possible gigabit)


**Requisits recomanats:**

Descripció|Requisits recomanats del sistema
---|---
Arquitectura CPU  |  x64
CPU clock rate  | 2 GHz  
RAM  | 8GB
Espai de disc  | 256GB
Adaptador de xarxa  | 1x Ethernet (si es possible gigabit)


Fes que el servidor es converteixi amb segon controlador del teu domini. Utilitza l'eina Dominios y Confianzas per mostrar en una captura el nivell funcional del bosc i del domini.
![image](Images/1.png)

Comprova que s'han replicat tots els objectes del AD al nou servidor: Unitats organitzatives, usuaris, grups, …

![image](Images/2.png)

Configura els equips clients de manera que utilitzin com a DNS alternatiu aquest nou servidor.
